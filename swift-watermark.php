<?php

 /**
 
 * Plugin Name:       Swift Watarmark
 * Plugin URI:        https://codember.com
 * Description:       Add Watermark to your WooCommerce products image.
 * Version:           1.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Codember
 * Author URI:        https://codember.com
 * Text Domain:       swift-watermark
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */


 function swift_watermark_assets($hook) {
 
    // create my own version codes
    // $my_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'js/custom.js' ));
    // $my_css_ver = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'style.css' ));
     
    // 
    wp_enqueue_script( 'watermark', plugins_url( 'jquery.watermark.js', __FILE__ ), array('jquery'), '1.0', true );
    wp_enqueue_script( 'main', plugins_url( 'main.js', __FILE__ ), array('jquery'), '1.0', true );
    // wp_register_style( 'my_css',    plugins_url( 'style.css',    __FILE__ ), false,   $my_css_ver );
    // wp_enqueue_style ( 'my_css' );
 
}
add_action('wp_enqueue_scripts', 'swift_watermark_assets');